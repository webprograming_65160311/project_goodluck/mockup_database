import { AppDataSource } from "./data-source";
import { Salary } from "./entity/Salary";

AppDataSource.initialize()
  .then(async () => {
    const salaryRepository = AppDataSource.getRepository(Salary);

    await salaryRepository.clear();

    var salary = new Salary();
    salary.id = 1;
    salary.payDate = new Date(2024, 2, 1, 8, 0, 59);
    salary.payMounth = new Date(2024, 2, 1, 16, 0, 59);
    salary.status = "Paid";
    salary.amount = 20000;
    console.log("Inserting a new user into the Database...", salary);
    await salaryRepository.save(salary);

    var salary = new Salary();
    salary.id = 2;
    salary.payDate = new Date(2024, 2, 1, 8, 0, 59);
    salary.payMounth = new Date(2024, 2, 1, 16, 0, 59);
    salary.status = "Not yet";
    salary.amount = 0;
    console.log("Inserting a new user into the Database...", salary);
    await salaryRepository.save(salary);

    const salarys = await salaryRepository.find({ order: { id: "asc" } });
    console.log(JSON.stringify(salarys, null, 2));
  })
  .catch((error) => console.log(error));
