import { AppDataSource } from "./data-source";
import { TimeAttendance } from "./entity/TimeAttendance";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const checkInOutRepository = AppDataSource.getRepository(TimeAttendance);
    const usersRepository = AppDataSource.getRepository(User);
    await checkInOutRepository.clear();
    const userId1 = await usersRepository.findOneBy({ id: 1 });
    const userId2 = await usersRepository.findOneBy({ id: 2 });

    console.log("Inserting a new user into the Memory...");
    var checkInOut = new TimeAttendance();
    checkInOut.id = 1;
    checkInOut.checkIn = new Date(2024, 2, 1, 8, 0, 59);
    checkInOut.checkOut = new Date(2024, 2, 1, 16, 0, 59);
    checkInOut.status = "ทันเวลา";
    checkInOut.hour = 8;
    checkInOut.user = userId1;
    console.log("Inserting a new user into the Database...", checkInOut);
    await checkInOutRepository.save(checkInOut);

    console.log("Inserting a new user into the Memory...");
    var checkInOut = new TimeAttendance();
    checkInOut.id = 2;
    checkInOut.checkIn = new Date(2024, 2, 1, 8, 5, 59);
    checkInOut.checkOut = new Date(2024, 2, 1, 16, 5, 59);
    checkInOut.status = "ทันเวลา";
    checkInOut.hour = 8;
    checkInOut.user = userId2;
    console.log("Inserting a new user into the Database...", checkInOut);
    await checkInOutRepository.save(checkInOut);

    console.log("Inserting a new user into the Memory...");
    var checkInOut = new TimeAttendance();
    checkInOut.id = 3;
    checkInOut.checkIn = new Date(2024, 2, 1, 8, 5, 59);
    checkInOut.checkOut = null;
    checkInOut.status = "ทันเวลา";
    checkInOut.hour = null;
    checkInOut.user = userId1;
    console.log("Inserting a new user into the Database...", checkInOut);
    await checkInOutRepository.save(checkInOut);
  })
  .catch((error) => console.log(error));
