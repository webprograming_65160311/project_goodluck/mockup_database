import { AppDataSource } from "./data-source";
import { Category } from "./entity/Category";
import { CheckRawMaterialDetail } from "./entity/CheckRawMaterialDetail";
import { CheckStock } from "./entity/CheckStock";
import { Member } from "./entity/Member";
import { OrderBill } from "./entity/OrderBill";
import { OrderBillDetail } from "./entity/OrderBillDetail";
import { Product } from "./entity/Product";
import { Promotion } from "./entity/Promotion";
import { RawMaterial } from "./entity/RawMaterial";
import { Receipt } from "./entity/Receipt";
import { ReceiptItem } from "./entity/ReceiptItem";
import { Role } from "./entity/Role";
import { Size } from "./entity/Size";
import { TimeAttendance } from "./entity/TimeAttendance";
import { Type } from "./entity/Type";
import { TypeUtilityBill } from "./entity/TypeUtilityBill";
import { User } from "./entity/User";
import { UtilityBill } from "./entity/UtilityBill";

AppDataSource.initialize()
  .then(async () => {
    await AppDataSource.manager.clear(CheckRawMaterialDetail);
    await AppDataSource.manager.clear(CheckStock);
    await AppDataSource.manager.clear(RawMaterial);
    await AppDataSource.manager.clear(ReceiptItem);
    await AppDataSource.manager.clear(Receipt);
    await AppDataSource.manager.clear(Member);
    await AppDataSource.manager.clear(TimeAttendance);
    await AppDataSource.manager.clear(User);
    await AppDataSource.manager.clear(Role);
    await AppDataSource.manager.clear(Promotion);
    await AppDataSource.manager.clear(Product);
    await AppDataSource.manager.clear(Type);
    await AppDataSource.manager.clear(Size);
    await AppDataSource.manager.clear(Category);
    await AppDataSource.manager.clear(OrderBill);
    await AppDataSource.manager.clear(OrderBillDetail);
    await AppDataSource.manager.clear(UtilityBill);
    await AppDataSource.manager.clear(TypeUtilityBill);
  })
  .catch((error) => console.log(error));
