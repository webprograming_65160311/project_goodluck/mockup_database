import { AppDataSource } from "../data-source";
import { Product } from "../entity/Product";
import { Promotion } from "../entity/Promotion";

AppDataSource.initialize()
  .then(async () => {
    const promoRepository = AppDataSource.getRepository(Promotion);
    await promoRepository.clear();

    //get products from database
    const productsRepository = AppDataSource.getRepository(Product);
    const product1 = await productsRepository.findOneBy({ name: "Americano" });
    const product2 = await productsRepository.findOneBy({ name: "Mocha" });
    const product3 = await productsRepository.findOneBy({
      name: "Chocolate cake",
    });

    //create promotion
    var promotion = new Promotion();
    promotion.id = 1;
    promotion.name = "Double Products";
    promotion.startDate = new Date();
    promotion.endDate = new Date(2024, 3, 1);
    promotion.status = true;
    promotion.promoProducts = [product1];
    await promoRepository.save(promotion);

    console.log(promotion.endDate.toLocaleString());

    promotion = new Promotion();
    promotion.id = 2;
    promotion.name = "50% Sell";
    promotion.startDate = new Date();
    promotion.endDate = new Date(2024, 4, 1);
    promotion.status = true;
    promotion.promoProducts = [product2, product3];
    await promoRepository.save(promotion);

    console.log(promotion.endDate.toLocaleString());
  })
  .catch((error) => console.log(error));
