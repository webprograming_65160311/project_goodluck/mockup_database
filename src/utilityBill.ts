import { AppDataSource } from "./data-source";
import { TypeUtilityBill } from "./entity/TypeUtilityBill";
import { User } from "./entity/User";
import { UtilityBill } from "./entity/UtilityBill";

AppDataSource.initialize()
  .then(async () => {
    const utilityBillRepository = AppDataSource.getRepository(UtilityBill);
    const usersRepository = AppDataSource.getRepository(User);
    const typeUtilityBillRepository =
      AppDataSource.getRepository(TypeUtilityBill);

    const user1 = await usersRepository.findOneBy({ id: 2 });
    const user2 = await usersRepository.findOneBy({ id: 3 });

    const type1 = await typeUtilityBillRepository.findOneBy({ id: 1 });
    const type2 = await typeUtilityBillRepository.findOneBy({ id: 2 });

    await utilityBillRepository.clear();

    var utilityBill = new UtilityBill();
    utilityBill.id = 1;
    utilityBill.invoiceNumber = "123";
    utilityBill.date = new Date();
    utilityBill.amount = 1200;
    utilityBill.remark = "มีค่าปรับล่าช้าเนื่องจากชำระค่าน้ำเกินกำหนด";
    utilityBill.status = "จ่ายแล้ว";
    utilityBill.user = user1;
    utilityBill.type = type1;

    await utilityBillRepository.save(utilityBill);

    var utilityBill = new UtilityBill();
    utilityBill.invoiceNumber = "456";
    utilityBill.id = 2;
    utilityBill.date = new Date();
    utilityBill.amount = 500;
    utilityBill.status = "ยังไม่ได้จ่าย";
    utilityBill.user = user2;
    utilityBill.type = type2;

    await utilityBillRepository.save(utilityBill);
  })
  .catch((error) => console.log(error));
