import { AppDataSource } from "../data-source";
import { Role } from "../entity/Role";

AppDataSource.initialize()
  .then(async () => {
    const rolesRepository = AppDataSource.getRepository(Role);
    await rolesRepository.clear();
    //create roles
    var role = new Role();
    role.id = 1;
    role.name = "admin";
    await rolesRepository.save(role);

    role = new Role();
    role.id = 2;
    role.name = "user";
    await rolesRepository.save(role);

    role = new Role();
    role.id = 3;
    role.name = "owner";
    await rolesRepository.save(role);
  })
  .catch((error) => console.log(error));
