import { AppDataSource } from "../data-source";
import { CheckRawMaterialDetail } from "../entity/CheckRawMaterialDetail";
import { Role } from "../entity/Role";
import { User } from "../entity/User";

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    await usersRepository.clear();

    //get roles from database
    const rolesRepository = AppDataSource.getRepository(Role);
    const adminRole = await rolesRepository.findOneBy({ name: "admin" });
    const userRole = await rolesRepository.findOneBy({ name: "user" });
    const ownerRole = await rolesRepository.findOneBy({ name: "owner" });

    //create users
    var user = new User();
    user.id = 1;
    user.fullName = "Admin Worker";
    user.email = "admin@email.com";
    user.password =
      "$2a$10$gYkwVXAc5BnNKM/gj2dv1OqhYEwYrp6CRtUDcptxSRv3gwOYZTMqi";
    user.gender = "male";
    user.role = adminRole;
    await usersRepository.save(user);

    user = new User();
    user.id = 2;
    user.fullName = "User Worker";
    user.email = "user@email.com";
    user.password =
      "$2a$10$gYkwVXAc5BnNKM/gj2dv1OqhYEwYrp6CRtUDcptxSRv3gwOYZTMqi";
    user.gender = "female";
    user.role = userRole;
    await usersRepository.save(user);

    user = new User();
    user.id = 3;
    user.fullName = "Owner Worker";
    user.email = "owner@email.com";
    user.password =
      "$2a$10$gYkwVXAc5BnNKM/gj2dv1OqhYEwYrp6CRtUDcptxSRv3gwOYZTMqi";
    user.gender = "others";
    user.role = ownerRole;
    await usersRepository.save(user);
  })
  .catch((error) => console.log(error));
