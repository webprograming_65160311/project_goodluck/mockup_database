import { AppDataSource } from "../data-source";
import { Member } from "../entity/Member";
import { Product } from "../entity/Product";
import { Promotion } from "../entity/Promotion";

AppDataSource.initialize()
  .then(async () => {
    const membersRepository = AppDataSource.getRepository(Member);
    await membersRepository.clear();

    //create member
    var member = new Member();
    member.id = 1;
    member.name = "Adisai Jell";
    member.tel = "0888888888";
    member.point = 10;
    await membersRepository.save(member);

    member = new Member();
    member.id = 2;
    member.name = "Panatipata Veramanee";
    member.tel = "0888888889";
    member.point = 100;
    await membersRepository.save(member);

    member = new Member();
    member.id = 3;
    member.name = "Arahang Samma";
    member.tel = "0888888887";
    member.point = 50;
    await membersRepository.save(member);
  })
  .catch((error) => console.log(error));
