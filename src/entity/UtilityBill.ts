import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { TypeUtilityBill } from "./TypeUtilityBill";

@Entity()
export class UtilityBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  invoiceNumber: string;

  @Column()
  date: Date;

  @Column()
  amount: number;

  @Column({ nullable: true })
  remark: string;

  @Column()
  status: string;

  @ManyToOne(() => User, (user) => user.utilityBill, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => TypeUtilityBill, (type) => type.utilityBills, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  type: TypeUtilityBill;
}
