import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { TimeAttendance } from "./TimeAttendance";

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "datetime", default: () => "datetime('now')" })
  payDate: Date;

  @Column({ nullable: true, default: () => "datetime('now')" })
  payMounth: Date;

  @Column()
  status: string;

  @Column()
  amount: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => User, (user) => user.salary, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;

  @OneToMany(() => TimeAttendance, (ta) => ta.salary)
  timeAttendances: TimeAttendance[];
}
