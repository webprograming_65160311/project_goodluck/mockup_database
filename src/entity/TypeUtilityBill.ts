import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { UtilityBill } from "./UtilityBill";

@Entity()
export class TypeUtilityBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => UtilityBill, (utilityBills) => utilityBills.type)
  utilityBills: UtilityBill[];
}
