import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { Salary } from "./Salary";

@Entity()
export class TimeAttendance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "datetime", default: () => "datetime('now')" })
  checkIn: Date;

  @Column({ nullable: true, default: () => "datetime('now')" })
  checkOut: Date;

  @Column()
  status: string;

  @Column({ nullable: true })
  hour: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => User, (user) => user.timeAttendances, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;

  @ManyToOne(() => Salary, (salary) => salary.timeAttendances)
  salary: any;
}
