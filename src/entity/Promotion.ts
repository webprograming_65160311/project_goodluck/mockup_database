import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Product } from "./Product";
import { Receipt } from "./Receipt";

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  status: boolean;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Product, { onDelete: "CASCADE", onUpdate: "CASCADE" })
  @JoinTable()
  promoProducts: Product[];

  @OneToMany(() => Receipt, (receipt) => receipt.promotion)
  receipts: Receipt[];
}
