import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import { CheckStock } from "./CheckStock";
import { RawMaterial } from "./RawMaterial";

@Entity()
export class CheckRawMaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bfCheck: number;

  @Column()
  atCheck: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => RawMaterial,
    (rawMaterial) => rawMaterial.checkRawMaterialDetails,
    { onDelete: "CASCADE" }
  )
  rawMaterial: RawMaterial;

  @ManyToOne(
    () => CheckStock,
    (checkStock) => checkStock.checkRawMaterialDetails
  )
  checkStock: CheckStock;
}
