import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Category } from "./Category";
import { Size } from "./Size";
import { Type } from "./Type";
import { ReceiptItem } from "./ReceiptItem";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Category, (category) => category.products, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  category: Category;

  @ManyToMany(() => Size)
  @JoinTable()
  sizes: Size[];

  @ManyToMany(() => Type)
  @JoinTable()
  type: Type[];

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.product)
  receiptItems: ReceiptItem[];
}
