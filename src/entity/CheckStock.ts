import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
} from "typeorm";
import { RawMaterial } from "./RawMaterial";
import { CheckRawMaterialDetail } from "./CheckRawMaterialDetail";
import { User } from "./User";

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  checkDate: Date;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => CheckRawMaterialDetail, (detail) => detail.checkStock)
  checkRawMaterialDetails: CheckRawMaterialDetail[];

  @ManyToOne(() => User, (user) => user.checkStock, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;
}
