import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from "typeorm";
import { OrderBillDetail } from "./OrderBillDetail";
import { User } from "./User";

@Entity()
export class OrderBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  Store: string;

  @Column()
  totalPrice: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => OrderBillDetail, (detail) => detail.OrderBill)
  orderBillDetails: OrderBillDetail[];

  @ManyToOne(() => User, (user) => user.orderBill, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;
}
