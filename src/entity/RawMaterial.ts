import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  OneToMany,
} from "typeorm";
import { CheckStock } from "./CheckStock";
import { CheckRawMaterialDetail } from "./CheckRawMaterialDetail";

@Entity()
export class RawMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  quantity: number;

  @Column()
  price: number;

  @Column()
  minimum: number;

  @Column()
  unit: string;

  @Column()
  status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => CheckRawMaterialDetail, (detail) => detail.checkStock)
  checkRawMaterialDetails: CheckRawMaterialDetail[];
}
