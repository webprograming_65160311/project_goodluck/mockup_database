import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import { CheckStock } from "./CheckStock";
import { RawMaterial } from "./RawMaterial";
import { OrderBill } from "./OrderBill";

@Entity()
export class OrderBillDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column()
  unitPrice: number;

  @Column()
  totalPrice: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => RawMaterial,
    (rawMaterial) => rawMaterial.checkRawMaterialDetails,
    { onDelete: "CASCADE" }
  )
  rawMaterial: RawMaterial;

  @ManyToOne(() => OrderBill, (orderBill) => orderBill.orderBillDetails)
  OrderBill: OrderBill;
}
