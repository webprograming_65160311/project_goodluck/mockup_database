import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Role } from "./Role";
import { CheckStock } from "./CheckStock";
import { Receipt } from "./Receipt";
import { TimeAttendance } from "./TimeAttendance";
import { Salary } from "./Salary";
import { OrderBill } from "./OrderBill";
import { UtilityBill } from "./UtilityBill";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Role, (role) => role.users, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  role: Role;

  @OneToMany(() => CheckStock, (CheckStock) => CheckStock.user)
  checkStock: CheckStock[];

  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];

  @OneToMany(() => TimeAttendance, (ta) => ta.user)
  timeAttendances: TimeAttendance[];

  @OneToMany(() => Salary, (salary) => salary.user)
  salary: Salary[];

  @OneToMany(() => OrderBill, (orderBill) => orderBill.user)
  orderBill: OrderBill[];

  @OneToMany(() => OrderBill, (utilityBill) => utilityBill.user)
  utilityBill: UtilityBill[];
}
