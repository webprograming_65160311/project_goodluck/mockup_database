import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Role } from "./Role";
import { Receipt } from "./Receipt";
import { Product } from "./Product";

@Entity()
export class ReceiptItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @Column({ nullable: true })
  size: string;

  @Column({ nullable: true })
  type: string;

  @Column({ nullable: true })
  sweetLv: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptItems, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptItems, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  product: Product;
}
