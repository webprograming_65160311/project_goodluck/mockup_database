import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Role } from "./Role";
import { ReceiptItem } from "./ReceiptItem";
import { User } from "./User";
import { Promotion } from "./Promotion";
import { Member } from "./Member";

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @Column()
  promoDiscount: number;

  @Column()
  memberDiscount: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.receipt)
  receiptItems: ReceiptItem[];

  @ManyToOne(() => User, (user) => user.receipts, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  user: User;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipts, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  promotion: Promotion;

  @ManyToOne(() => Member, (member) => member.receipts, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  member: Member;
}
