import "reflect-metadata";
import { DataSource } from "typeorm";
import { RawMaterial } from "./entity/RawMaterial";
import { CheckStock } from "./entity/CheckStock";
import { CheckRawMaterialDetail } from "./entity/CheckRawMaterialDetail";
import { User } from "./entity/User";
import { Role } from "./entity/Role";
import { Receipt } from "./entity/Receipt";
import { ReceiptItem } from "./entity/ReceiptItem";
import { Category } from "./entity/Category";
import { Size } from "./entity/Size";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";
import { Salary } from "./entity/Salary";
import { Promotion } from "./entity/Promotion";
import { Member } from "./entity/Member";
import { TimeAttendance } from "./entity/TimeAttendance";
import { OrderBill } from "./entity/OrderBill";
import { OrderBillDetail } from "./entity/OrderBillDetail";
import { UtilityBill } from "./entity/UtilityBill";
import { TypeUtilityBill } from "./entity/TypeUtilityBill";

export const AppDataSource = new DataSource({
  type: "sqlite",
  database: "database.sqlite",
  synchronize: true,
  logging: false,
  entities: [
    RawMaterial,
    CheckStock,
    CheckRawMaterialDetail,
    TimeAttendance,
    User,
    Role,
    Receipt,
    ReceiptItem,
    Product,
    Category,
    Size,
    Type,
    Salary,
    Promotion,
    Member,
    OrderBill,
    OrderBillDetail,
    UtilityBill,
    TypeUtilityBill,
  ],
});
