import { AppDataSource } from "../data-source";
import { Member } from "../entity/Member";
import { Product } from "../entity/Product";
import { Promotion } from "../entity/Promotion";
import { Receipt } from "../entity/Receipt";
import { ReceiptItem } from "../entity/ReceiptItem";
import { User } from "../entity/User";

const receiptDto = {
  receiptItems: [
    { productName: "Mocha", qty: 5 },
    { productName: "Americano", qty: 2 },
    { productName: "Fried rice", qty: 1 },
  ],
  userName: "Admin Worker",
};

AppDataSource.initialize()
  .then(async () => {
    const receiptsRepository = AppDataSource.getRepository(Receipt);
    const receiptItemsRepository = AppDataSource.getRepository(ReceiptItem);
    const productsRepository = AppDataSource.getRepository(Product);
    const membersRepository = AppDataSource.getRepository(Member);
    const promoRepository = AppDataSource.getRepository(Promotion);
    const usersRepositry = AppDataSource.getRepository(User);
    await receiptItemsRepository.clear();
    await receiptsRepository.clear();

    //create receipt
    const user = await usersRepositry.findOneBy({
      fullName: receiptDto.userName,
    });

    const receipt = new Receipt();
    receipt.id = 1;
    receipt.user = user;
    receipt.qty = 0;
    receipt.total = 0;
    receipt.memberDiscount = 0;
    receipt.promoDiscount = 0;
    receipt.receiptItems = [];

    for (const rcptItem of receiptDto.receiptItems) {
      const receiptItem = new ReceiptItem();
      receiptItem.product = await productsRepository.findOneBy({
        name: rcptItem.productName,
      });
      receiptItem.name = receiptItem.product.name;
      receiptItem.price = receiptItem.product.price;
      receiptItem.qty = rcptItem.qty;
      receiptItem.total = receiptItem.price * receiptItem.qty;
      await receiptItemsRepository.save(receiptItem);
      receipt.receiptItems.push(receiptItem);
      receipt.qty += receiptItem.qty;
      receipt.total += receiptItem.total;
    }
    await receiptsRepository.save(receipt);
  })
  .catch((error) => console.log(error));
