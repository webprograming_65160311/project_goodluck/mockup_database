import { AppDataSource } from "../data-source";
import { CheckRawMaterialDetail } from "../entity/CheckRawMaterialDetail";
import { CheckStock } from "../entity/CheckStock";

import { RawMaterial } from "../entity/RawMaterial";

AppDataSource.initialize()
  .then(async () => {
    const rawMaterialsRepository = AppDataSource.getRepository(RawMaterial);
    const checkStocksRepository = AppDataSource.getRepository(CheckStock);
    const checkDetailRepository = AppDataSource.getRepository(
      CheckRawMaterialDetail
    );

    await checkDetailRepository.clear();
    await checkStocksRepository.clear();
    await rawMaterialsRepository.clear();

    var rawMaterial = new RawMaterial();
    rawMaterial.id = 1;
    rawMaterial.name = "กาแฟอาราบิก้า";
    rawMaterial.quantity = 9;
    rawMaterial.price = 30;
    rawMaterial.minimum = 10;
    rawMaterial.unit = "กรัม";
    rawMaterial.status = "Low Stock";
    await rawMaterialsRepository.save(rawMaterial);

    rawMaterial.id = 2;
    rawMaterial.name = "กาแฟโรบัสต้า";
    rawMaterial.quantity = 20;
    rawMaterial.price = 25;
    rawMaterial.minimum = 10;
    rawMaterial.unit = "กรัม";
    rawMaterial.status = "Normal";
    await rawMaterialsRepository.save(rawMaterial);

    rawMaterial.id = 3;
    rawMaterial.name = "น้ำตาล";
    rawMaterial.quantity = 16;
    rawMaterial.price = 14;
    rawMaterial.minimum = 10;
    rawMaterial.unit = "กิโลกรัม";
    rawMaterial.status = "Normal";
    await rawMaterialsRepository.save(rawMaterial);

    rawMaterial.id = 4;
    rawMaterial.name = "นมข้นหวาน";
    rawMaterial.quantity = 4;
    rawMaterial.price = 25;
    rawMaterial.minimum = 10;
    rawMaterial.unit = "กรัม";
    rawMaterial.status = "Low Stock";

    await rawMaterialsRepository.save(rawMaterial);

    rawMaterial.id = 5;
    rawMaterial.name = "นมสด";
    rawMaterial.quantity = 10;
    rawMaterial.price = 27;
    rawMaterial.minimum = 5;
    rawMaterial.unit = "กระป๋อง";
    rawMaterial.status = "Normal";

    await rawMaterialsRepository.save(rawMaterial);
  })

  .catch((error) => console.log(error));
