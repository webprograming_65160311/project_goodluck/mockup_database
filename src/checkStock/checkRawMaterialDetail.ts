import { AppDataSource } from "../data-source";
import { CheckRawMaterialDetail } from "../entity/CheckRawMaterialDetail";

import { CheckStock } from "../entity/CheckStock";
import { RawMaterial } from "../entity/RawMaterial";

AppDataSource.initialize()
  .then(async () => {
    const checkStockRepository = AppDataSource.getRepository(CheckStock);
    const rawMaterialRepository = AppDataSource.getRepository(RawMaterial);
    const checkDetailRepository = AppDataSource.getRepository(
      CheckRawMaterialDetail
    );

    const checkStock1 = await checkStockRepository.findOneBy({ id: 1 });
    const checkStock2 = await checkStockRepository.findOneBy({ id: 2 });

    const rawMaterial1 = await rawMaterialRepository.findOneBy({ id: 1 });
    const rawMaterial2 = await rawMaterialRepository.findOneBy({ id: 2 });
    const rawMaterial3 = await rawMaterialRepository.findOneBy({ id: 3 });
    const rawMaterial4 = await rawMaterialRepository.findOneBy({ id: 4 });
    const rawMaterial5 = await rawMaterialRepository.findOneBy({ id: 5 });

    await checkDetailRepository.clear();

    var checkDetail = new CheckRawMaterialDetail();
    checkDetail.id = 1;
    checkDetail.bfCheck = rawMaterial1.quantity;
    checkDetail.atCheck = 3;
    checkDetail.rawMaterial = rawMaterial1;
    checkDetail.checkStock = checkStock1;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 2;
    checkDetail.bfCheck = rawMaterial2.quantity;
    checkDetail.atCheck = 3;
    checkDetail.rawMaterial = rawMaterial2;
    checkDetail.checkStock = checkStock1;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 3;
    checkDetail.bfCheck = rawMaterial3.quantity;
    checkDetail.atCheck = 5;
    checkDetail.rawMaterial = rawMaterial3;
    checkDetail.checkStock = checkStock1;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 4;
    checkDetail.bfCheck = rawMaterial4.quantity;
    checkDetail.atCheck = 4;
    checkDetail.rawMaterial = rawMaterial4;
    checkDetail.checkStock = checkStock1;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 5;
    checkDetail.bfCheck = rawMaterial5.quantity;
    checkDetail.atCheck = 2;
    checkDetail.rawMaterial = rawMaterial5;
    checkDetail.checkStock = checkStock1;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 6;
    checkDetail.bfCheck = rawMaterial1.quantity;
    checkDetail.atCheck = 10;
    checkDetail.rawMaterial = rawMaterial1;
    checkDetail.checkStock = checkStock2;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 7;
    checkDetail.bfCheck = rawMaterial2.quantity;
    checkDetail.atCheck = 0;
    checkDetail.rawMaterial = rawMaterial2;
    checkDetail.checkStock = checkStock2;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 8;
    checkDetail.bfCheck = rawMaterial3.quantity;
    checkDetail.atCheck = 1;
    checkDetail.rawMaterial = rawMaterial3;
    checkDetail.checkStock = checkStock2;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 9;
    checkDetail.bfCheck = rawMaterial4.quantity;
    checkDetail.atCheck = 2;
    checkDetail.rawMaterial = rawMaterial4;
    checkDetail.checkStock = checkStock2;

    await checkDetailRepository.save(checkDetail);

    checkDetail.id = 10;
    checkDetail.bfCheck = rawMaterial5.quantity;
    checkDetail.atCheck = 4;
    checkDetail.rawMaterial = rawMaterial5;
    checkDetail.checkStock = checkStock2;

    await checkDetailRepository.save(checkDetail);
  })

  .catch((error) => console.log(error));
