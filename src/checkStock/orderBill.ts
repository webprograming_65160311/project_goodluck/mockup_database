import { AppDataSource } from "../data-source";

import { OrderBill } from "../entity/OrderBill";
import { OrderBillDetail } from "../entity/OrderBillDetail";
import { RawMaterial } from "../entity/RawMaterial";
import { User } from "../entity/User";

AppDataSource.initialize()
  .then(async () => {
    const orderBillRepository = AppDataSource.getRepository(OrderBill);
    const rawMaterialRepository = AppDataSource.getRepository(RawMaterial);
    const usersRepository = AppDataSource.getRepository(User);

    const user1 = await usersRepository.findOneBy({ id: 2 });
    const user2 = await usersRepository.findOneBy({ id: 3 });

    const rawMaterial1 = await rawMaterialRepository.findOneBy({ id: 1 });
    const rawMaterial2 = await rawMaterialRepository.findOneBy({ id: 2 });
    const rawMaterial3 = await rawMaterialRepository.findOneBy({ id: 3 });

    await orderBillRepository.clear();

    var orderBill1 = new OrderBill();
    orderBill1.id = 1;
    orderBill1.date = new Date();
    orderBill1.totalPrice = 270;
    orderBill1.orderBillDetails = [];
    orderBill1.Store = "Big C";
    orderBill1.user = user1;

    const detail1 = new OrderBillDetail();
    detail1.rawMaterial = rawMaterial1;
    orderBill1.orderBillDetails.push(detail1);

    const detail2 = new OrderBillDetail();
    detail1.rawMaterial = rawMaterial2;
    orderBill1.orderBillDetails.push(detail2);

    const detail3 = new OrderBillDetail();
    detail1.rawMaterial = rawMaterial3;
    orderBill1.orderBillDetails.push(detail3);

    await orderBillRepository.save(orderBill1);

    var orderBill2 = new OrderBill();
    orderBill2.id = 2;
    orderBill2.date = new Date();
    orderBill2.totalPrice = 85;
    orderBill2.orderBillDetails = [];
    orderBill2.Store = "7-11";
    orderBill2.user = user2;

    const detail4 = new OrderBillDetail();
    detail3.rawMaterial = rawMaterial1;
    orderBill2.orderBillDetails.push(detail4);

    const detail5 = new OrderBillDetail();
    detail3.rawMaterial = rawMaterial2;
    orderBill2.orderBillDetails.push(detail5);

    await orderBillRepository.save(orderBill2);
  })
  .catch((error) => console.log(error));
