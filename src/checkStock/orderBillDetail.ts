import { AppDataSource } from "../data-source";
import { OrderBill } from "../entity/OrderBill";
import { OrderBillDetail } from "../entity/OrderBillDetail";
import { RawMaterial } from "../entity/RawMaterial";

AppDataSource.initialize()
  .then(async () => {
    const orderBillRepository = AppDataSource.getRepository(OrderBill);
    const rawMaterialRepository = AppDataSource.getRepository(RawMaterial);
    const orderBillDetailRepository =
      AppDataSource.getRepository(OrderBillDetail);

    const orderBill1 = await orderBillRepository.findOneBy({ id: 1 });
    const orderBill2 = await orderBillRepository.findOneBy({ id: 2 });

    const rawMaterial1 = await rawMaterialRepository.findOneBy({ id: 1 });
    const rawMaterial2 = await rawMaterialRepository.findOneBy({ id: 2 });
    const rawMaterial3 = await rawMaterialRepository.findOneBy({ id: 3 });

    await orderBillDetailRepository.clear();

    var orderBillDetail = new OrderBillDetail();
    orderBillDetail.id = 1;
    orderBillDetail.quantity = 10;
    orderBillDetail.unitPrice = 10;
    orderBillDetail.totalPrice =
      orderBillDetail.unitPrice * orderBillDetail.quantity;
    orderBillDetail.rawMaterial = rawMaterial1;
    orderBillDetail.OrderBill = orderBill1;

    await orderBillDetailRepository.save(orderBillDetail);

    orderBillDetail.id = 2;
    orderBillDetail.quantity = 13;
    orderBillDetail.unitPrice = 10;
    orderBillDetail.totalPrice =
      orderBillDetail.unitPrice * orderBillDetail.quantity;
    orderBillDetail.rawMaterial = rawMaterial2;
    orderBillDetail.OrderBill = orderBill1;

    await orderBillDetailRepository.save(orderBillDetail);

    orderBillDetail.id = 3;
    orderBillDetail.quantity = 4;
    orderBillDetail.unitPrice = 10;
    orderBillDetail.totalPrice =
      orderBillDetail.unitPrice * orderBillDetail.quantity;
    orderBillDetail.rawMaterial = rawMaterial3;
    orderBillDetail.OrderBill = orderBill1;

    await orderBillDetailRepository.save(orderBillDetail);

    orderBillDetail.id = 4;
    orderBillDetail.quantity = 5;
    orderBillDetail.unitPrice = 5;
    orderBillDetail.totalPrice =
      orderBillDetail.unitPrice * orderBillDetail.quantity;

    orderBillDetail.rawMaterial = rawMaterial1;
    orderBillDetail.OrderBill = orderBill2;

    await orderBillDetailRepository.save(orderBillDetail);

    orderBillDetail.id = 5;
    orderBillDetail.quantity = 6;
    orderBillDetail.unitPrice = 10;
    orderBillDetail.totalPrice =
      orderBillDetail.unitPrice * orderBillDetail.quantity;
    orderBillDetail.rawMaterial = rawMaterial2;
    orderBillDetail.OrderBill = orderBill2;

    await orderBillDetailRepository.save(orderBillDetail);
  })

  .catch((error) => console.log(error));
