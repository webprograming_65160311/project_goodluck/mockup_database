import { AppDataSource } from "../data-source";
import { CheckStock } from "../entity/CheckStock";
import { User } from "../entity/User";

AppDataSource.initialize()
  .then(async () => {
    const checkStocksRepository = AppDataSource.getRepository(CheckStock);

    const usersRepository = AppDataSource.getRepository(User);

    const user1 = await usersRepository.findOneBy({ id: 2 });
    const user2 = await usersRepository.findOneBy({ id: 3 });

    await checkStocksRepository.clear();

    const checkStock1 = new CheckStock();
    checkStock1.id = 1;
    checkStock1.checkDate = new Date();
    checkStock1.user = user1;
    checkStock1.checkRawMaterialDetails = [];

    await checkStocksRepository.save(checkStock1);

    const checkStock2 = new CheckStock();
    checkStock2.id = 2;
    checkStock2.checkDate = new Date();
    checkStock2.user = user2;
    checkStock2.checkRawMaterialDetails = [];

    await checkStocksRepository.save(checkStock2);
  })
  .catch((error) => console.log(error));
