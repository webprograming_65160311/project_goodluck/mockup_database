import { AppDataSource } from "./data-source";
import { TypeUtilityBill } from "./entity/TypeUtilityBill";

AppDataSource.initialize()
  .then(async () => {
    const typeUtilityBillRepository =
      AppDataSource.getRepository(TypeUtilityBill);

    await typeUtilityBillRepository.clear();

    var typeUtilityBill = new TypeUtilityBill();
    typeUtilityBill.id = 1;
    typeUtilityBill.name = "ค่าน้ำ";

    await typeUtilityBillRepository.save(typeUtilityBill);

    var typeUtilityBill = new TypeUtilityBill();
    typeUtilityBill.id = 2;
    typeUtilityBill.name = "ค่าไฟ";

    await typeUtilityBillRepository.save(typeUtilityBill);
  })
  .catch((error) => console.log(error));
