import { AppDataSource } from "../data-source";
import { Category } from "../entity/Category";
import { Product } from "../entity/Product";
import { Size } from "../entity/Size";
import { Type } from "../entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const productsRepository = AppDataSource.getRepository(Product);
    await productsRepository.clear();

    //get types from database
    const typesRepository = AppDataSource.getRepository(Type);
    const hot = await typesRepository.findOneBy({ name: "h" });
    const cold = await typesRepository.findOneBy({ name: "c" });
    const freppe = await typesRepository.findOneBy({ name: "f" });

    //get categores from database
    const categoriesRepository = AppDataSource.getRepository(Category);
    const drink = await categoriesRepository.findOneBy({ name: "drink" });
    const dessert = await categoriesRepository.findOneBy({
      name: "dessert",
    });
    const food = await categoriesRepository.findOneBy({ name: "food" });

    //get size from database
    const sizesRepository = AppDataSource.getRepository(Size);
    const s = await sizesRepository.findOneBy({ name: "s" });
    const m = await sizesRepository.findOneBy({
      name: "m",
    });
    const l = await sizesRepository.findOneBy({ name: "l" });

    //create product
    var product = new Product();
    product.id = 1;
    product.name = "Mocha";
    product.price = 35.0;
    product.category = drink;
    product.sizes = [s, m, l];
    product.type = [hot, cold, freppe];
    await productsRepository.save(product);

    product = new Product();
    product.id = 2;
    product.name = "Americano";
    product.price = 55.0;
    product.category = drink;
    product.sizes = [s, m, l];
    product.type = [hot, cold];
    await productsRepository.save(product);

    product = new Product();
    product.id = 3;
    product.name = "Chocolate cake";
    product.price = 55.0;
    product.category = dessert;
    await productsRepository.save(product);

    product = new Product();
    product.id = 4;
    product.name = "Fried rice";
    product.price = 35.0;
    product.category = food;
    await productsRepository.save(product);
  })
  .catch((error) => console.log(error));
