import { AppDataSource } from "../data-source";
import { Type } from "../entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    await typesRepository.clear();

    //create type
    var type = new Type();
    type.id = 1;
    type.name = "h";
    await typesRepository.save(type);

    type = new Type();
    type.id = 2;
    type.name = "c";
    await typesRepository.save(type);

    type = new Type();
    type.id = 3;
    type.name = "f";
    await typesRepository.save(type);
  })
  .catch((error) => console.log(error));
