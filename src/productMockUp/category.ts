import { AppDataSource } from "../data-source";
import { Category } from "../entity/Category";

AppDataSource.initialize()
  .then(async () => {
    const categoriesRepository = AppDataSource.getRepository(Category);
    await categoriesRepository.clear();

    //create category
    var category = new Category();
    category.id = 1;
    category.name = "drink";
    await categoriesRepository.save(category);

    category = new Category();
    category.id = 2;
    category.name = "dessert";
    await categoriesRepository.save(category);

    category = new Category();
    category.id = 3;
    category.name = "food";
    await categoriesRepository.save(category);
  })
  .catch((error) => console.log(error));
