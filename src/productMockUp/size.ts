import { AppDataSource } from "../data-source";
import { Size } from "../entity/Size";

AppDataSource.initialize()
  .then(async () => {
    const sizesRepository = AppDataSource.getRepository(Size);
    await sizesRepository.clear();

    //create size
    var size = new Size();
    size.id = 1;
    size.name = "s";
    await sizesRepository.save(size);

    size = new Size();
    size.id = 2;
    size.name = "m";
    await sizesRepository.save(size);

    size = new Size();
    size.id = 3;
    size.name = "l";
    await sizesRepository.save(size);
  })
  .catch((error) => console.log(error));
